# Migrazione dominio tecnico

Il presente documento descrive l'attività di migrazione delle postazioni di lavoro e delle risorse IT dell'area tecnica (server, storage, ecc.) all'interno del dominio aziendale, riportandone la gestione sotto il controllo del personale IT aziendale incaricato.

Tale migrazione avrà il vantaggio di apportare miglioramenti sul fronte della sicurezza informatica e libererà il personale dell'area tecnica da tale incombenza lasciandolo all'esercizio della propria attività istituzionale.

La migrazione dovrà inoltre garantire a tutto il personale dell'area tecnica il prosieguo della propria attività senza l'occorrenza di vincoli operativi di sorta.

Allo scopo di garantire tale requisito e per non incorrere in inattesi blocchi operativi, sarà effettuata un'operazione di simulazione per verificare la compatibilità delle policy informatiche aziendali e delle necessità operative dell'area tecnica nell'esercizio della propria attività di ricerca e sviluppo.

## Obiettivi della simulazione

- [ ] Autonomia di postazione
- [ ] Accesso allo storage locale
- [ ] Accesso ai server software locali
- [ ] Gestione sotto-rete tecnica

## Risorse necessarie

## 
---
